<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
        // Делаем перенаправление на форму.
        header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    ?>
<div class="cover-block"></div>
<form class="transparent" action="" method="post">
<div class="form">
  <label>
  <p>Login: </p>
   <input name="login" />
  </label>
  <label>
  <p>Password: </p>
   <input name="pass" />
   </label>
   <label>
  <input id="log" type="submit" value="Sign in" />
  </label>
  </div>
</form>
</div>

<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
  $user = 'u20307';
  $pass = '8543732';  
  $log= $_SESSION['login'];
  $pas=$_SESSION['password'];
$sql = "SELECT fio FROM application WHERE login ='$log' AND password = '$pas'";
$link =  mysqli_connect("localhost", "u20307", $user, $pass);
$result = mysqli_query($link, $sql);

$row = mysqli_fetch_array($result);
if(empty($row['fio']))
    echo('<script>alert("user has been created early");</script>');
else{
  // Если все ок, то авторизуем пользователя.
  $_SESSION['login'] = $_POST['login'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = mt_rand(0,1000);
}
  // Делаем перенаправление.
  header('Location: ./');
}

