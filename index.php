<?php
//  Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        ?><script> alert('Thank you, the results are saved.');</script>
      <?php
   // Если в куках есть пароль, то выводим сообщение.
   if (!empty($_COOKIE['pass'])) {
    $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
      и паролем <strong>%s</strong> для изменения данных.',
      strip_tags($_COOKIE['login']),
      strip_tags($_COOKIE['pass']));
  }
}
  

// Складываем признак ошибок в массив.
$errors = array();
$errors['fio'] = !empty($_COOKIE['fio_error']);
$errors['email'] = !empty($_COOKIE['email_error']);
$errors['date'] = !empty($_COOKIE['date_error']);
$errors['sex'] = !empty($_COOKIE['sex_error']);
$errors['edu'] = !empty($_COOKIE['edu_error']);
$errors['course'] = !empty($_COOKIE['course_error']);
$errors['comment'] = !empty($_COOKIE['comment_error']);
$errors['check'] = !empty($_COOKIE['check_error']);

// Выдаем сообщения об ошибках.
if ($errors['fio']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('fio_error', '', 100000);
  // Выводим сообщение.
  if ($errors['fio'] == 'empty') $messages['fio'] = '<div class="errors">Напишите имя.</div>';
 else $messages['fio'] = '<div class="errors">Only letters and white space allowed.</div>';
}
if ($errors['email']) {
    setcookie('email_error', '', 100000);
   if($errors['email'] == 'empty') $messages['email'] = '<div class="errors">Email не заполнено.</div>';
   else $messages['email'] = '<div class="errors">Invalid email format.</div>';
  }
if ($errors['date']) {
    setcookie('date_error', '', 100000);
   $messages['date'] = '<div class="errors">Дата не заполнена.</div>';
  }
if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
   $messages['sex'] = '<div class="errors">Пол не выбран.</div>';
  }
if ($errors['edu']) {
    setcookie('edu_error', '', 100000);
   $messages['edu'] = '<div class="errors">Коннечности не выбраны.</div>';
  }
if ($errors['course']) {
    setcookie('course_error', '', 100000);
   $messages['course'] = '<div class="errors">Способность не выбрана.</div>';
  }
if ($errors['comment']) {
    setcookie('comment_error', '', 100000);
   $messages['comment'] = '<div class="errors">Биографиия не заполнена.</div>';
  }
if ($errors['check']) {
    setcookie('check_error', '', 100000);
   $messages['check'] = '<div class="errors">Галочка не стоит.</div>';
  }

// Складываем предыдущие значения полей в массив, если есть.
 // При этом санитизуем все данные для безопасного отображения в браузере.
$values = array();
$values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
$values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
$values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
$values['edu'] = empty($_COOKIE['edu_value']) ? '' : strip_tags($_COOKIE['edu_value']);
$values['course'] = empty($_COOKIE['course_value']) ? '' : strip_tags($_COOKIE['course_value']);
$values['comment'] = empty($_COOKIE['comment_value']) ? '' : strip_tags($_COOKIE['comment_value']);
$values['check'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);

// Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.
    $user = 'u20307';
    $pass = '85437326';
  //  $db = new PDO('mysql:host=localhost;dbname=u20378', $user, $pass,
    //    array(PDO::ATTR_PERSISTENT => true));
$log=$_SESSION['login'];    
$sql = "SELECT * FROM application WHERE login='$log'";
$link =  mysqli_connect("localhost", "u20307", $user, $pass);
$result = mysqli_query($link, $sql);

$row = mysqli_fetch_array($result);

 $course1 = strip_tags($row['Speed']) == 1 ? 'Speed' : '';
 $course2 = strip_tags($row['Power']) == 1 ? 'Power' : '';
 $course3 = strip_tags($row['PHP']) == 1 ? 'Fly' : '';
 $course4 = strip_tags($row['Nichego']) == 1 ? 'Nichego' : '';

    $values['fio'] = empty($row['fio']) ? '' : strip_tags($row['fio']);
    $values['email'] = empty($row['email']) ? '' : strip_tags($row['email']);
    $values['date'] = empty($row['date']) ? '' : strip_tags($row['date']);
    $values['sex'] = empty($row['sex']) ? '' : strip_tags($row['sex']);
    $values['edu'] = empty($row['education']) ? '' : strip_tags($row['education']);
    $values['course'] = $course1 . $course2 . $course3 . $course4;
    $values['comment'] = empty($row['comment']) ? '' : strip_tags($row['comment']);
    $values['check'] = true;

    ?><script>('You can enter with login %s, password %d', $_SESSION['login'], $_SESSION['uid']);</script>
    <?php
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else{
$errors = FALSE;

if (empty($_POST['fio'])) {
    // Выдаем куку до конца сессии с флажком об ошибке в поле fio.
    setcookie('fio_error', 'empty');
  $errors = TRUE;
}
else { if (!preg_match('/^[A-Za-zа-яА-Я\s]+$/iu',$_POST['fio'])) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
       }
    }

if (empty($_POST['email'])) {
    setcookie('email_error', 'empty');
    $errors = TRUE;
}
else { if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
       }
     }

if (empty($_POST['date'])) {
    setcookie('date_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['sex']){
    setcookie('sex_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['edu']){
    setcookie('edu_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('edu_value', $_POST['edu'], time() + 365 * 24 * 60 * 60);
  }


if (empty($_POST['course'])) {
    setcookie('course_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('course_value', serialize($_POST['course']), time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['comment']){
    setcookie('comment_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('comment_value', $_POST['comment'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['check']){
    setcookie('check_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('check_value', $_POST['check'], time() + 365 * 24 * 60 * 60);
  }



if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('edu_error', '', 100000);
    setcookie('course_error', '', 100000);
    setcookie('comment_error', '', 100000);
    setcookie('check_error', '', 100000);
  }

  $user = 'u20307';
  $pass = '8543732';
  $db = new PDO('mysql:host=localhost;dbname=u20307', $user, $pass,
      array(PDO::ATTR_PERSISTENT => true));
  

   // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
   if (!empty($_COOKIE[session_name()]) &&
   session_start() && !empty($_SESSION['login'])) {
 //перезаписать данные в БД новыми данными,
 // кроме логина и пароля.
 $ability1 = in_array('Speed', $_POST['course']) ? 1 : 0;
 $ability2 = in_array('Power', $_POST['course']) ? 1 : 0;
 $ability3 = in_array('Fly', $_POST['course']) ? 1 : 0;
 $ability4 = in_array('Nichego', $_POST['course']) ? 1 : 0;
 $log=$_SESSION['login'];  
 try {$stmt = $db->prepare("UPDATE application SET fio = ?, email = ?, date = ?, sex = ?, education = ?, Speed = ?, Power = ?, Fly = ?, Nichego = ?, comment = ? WHERE login = '$log'");
  $stmt->execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$_POST['sex'],$_POST['edu'],$ability1,$ability2,$ability3,$ability4,$_POST['comment']));
  }
  catch(PDOException $e){
      print('Error : failed to connect to database' . $e->getMessage());
      exit();
  }

}
else {
 // Генерируем уникальный логин и пароль.
 function login_generation() {
  $new_login = str(mt_rand(1000,9999));
  $new_login = chr(mt_rand(97, 122));
  return $new_login;
 }

 function pass_generation($length) {
  $new_password = '';
  for ($i = 0; $i < $length; $i++){
     $case = mt_rand(0, 2);
     if($case == 0)
     $new_password .= chr(mt_rand(48, 57));// 48 - это 0, а 57 - 9
     else {
       if($case == 1)
        $new_password .= chr(mt_rand(65, 90)); // 65 - это A, а 90 - Z
       else
        $new_password .= chr(mt_rand(97, 122)); // 97 - это a, а 122 - z
     }
    }
  return $new_password;
 }

 $login = login_generation();
 $password = pass_generation(5);
 // Сохраняем в Cookies.
 setcookie('login', $login);
 setcookie('pass', $password);
  
 //Сохранение данных формы, логина и хеш md5() пароля в базу данных.
  
 $ability1 = in_array('Speed', $_POST['course']) ? 1 : 0;
 $ability2 = in_array('Power', $_POST['course']) ? 1 : 0;
 $ability3 = in_array('Fly', $_POST['course']) ? 1 : 0;
 $ability4 = in_array('C', $_POST['course']) ? 1 : 0;

  //Подготовленный запрос
  //неименованные метки
  
  try {$stmt = $db->prepare("INSERT INTO application SET login = ?, password = ?, fio = ?, email = ?, date = ?, sex = ?, education = ?, Speed = ?, Power = ?, Fly = ?, Nichego = ?, comment = ?");
  $stmt->execute(array($login,md5($password),$_POST['fio'],$_POST['email'],$_POST['date'],$_POST['sex'],$_POST['edu'],$ability1,$ability2,$ability3,$ability4,$_POST['comment']));
  }
  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }

}
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}