`<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1">
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <style>
body{
  background-color:#F2F5A9;
}
.main-form{
    width:60%;
    display: flex;
    background-color:#F2F5A9;
  justify-content:center;
  border-radius:20px; 
  margin-left:20%;
}
.page{
  font-family: courier;
  background:#F2F5A9;
  overflow: hidden;
  text-decoration:none;
  font-size: 15px;
  text-align: center;
  }
  .page li{list-style:none;}
.navbar-brand{
  width:60px;
  }
form{
    text-align: center;
}
.errors {
  color: red;
}

.error {
  border: 2px solid red;
}
</style>
    <div class="cover-block"> 
<form class="transparent" action="" method="POST">
<div class="form">
<h3>-Formaa-</h3>
<label>
       <p>Представьтесь : </p> 
   <input type="textfield" name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" >
   <?php if (!empty($messages['fio'])) {print($messages['fio']);} ?>
   </label>
  <label>
       <p>Введите e-mail: </p>
   <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email" placeholder="test@example.com"> 
   <?php if (!empty($messages['email'])) {print($messages['email']);} ?>
      </label>
  <label>
      <p>Ваша дата рождения: </p>
  <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="date" min="1920-01-01" max="2005-01-01">
  <?php if (!empty($messages['date'])) {print($messages['date']);} ?>
      </label>
     <label>
      <p>Выберите пол<br>
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'Nichegolass="error"';} ?> checked="<?php if($values['sex'] == 'woman'){print 'checked';} ?>" type="radio" value="woman"> Woman</label> 
      <label class="radio"><input name="sex" <?php if ($errors['sex']) {print 'class="error"';} ?> checked="<?php if($values['sex'] == 'man'){print 'checked';} ?>" type="radio" value="man"> Man</label></br></p>
      <?php if (!empty($messages['sex'])) {print($messages['sex']);} ?>
 </label> 
 <label>
      <p>Количество конечностей: <br>
    <label class="radio"><input name="edu" type="radio" value="1" checked="<?php if($values['edu'] == '1') {print 'checked';} ?>"> 1</label>
      <label class="radio"><input name="edu" type="radio" value="2" checked="<?php if($values['edu'] == '2') {print 'checked';} ?>"> 2</label>
      <label class="radio"><input name="edu" type="radio" value="3" checked="<?php if($values['edu'] == '3') {print 'checked';} ?>"> 3</label>
      <label class="radio"><input name="edu" type="radio" value="4" checked="<?php if($values['edu'] == '4') {print 'checked';} ?>"> 4</label>
      <label class="radio"><input name="edu" type="radio" value="5" checked="<?php if($values['edu'] == '5') {print 'checked';} ?>"> 5</label><br></p>
      <?php if (!empty($messages['edu'])) {print($messages['edu']);} ?>
 </label>
 <label>
      <p>Выберите вашу способность:</p>
  <select name="course[]" multiple="multiple" <?php if ($errors['check']) {print 'class="error"';} ?>>
 <option value="Speed" <?php if($values['course']='Speed') {print 'selected';} ?> >Speed</option>
 <option value="Power" <?php if($values['course']='Power') {print 'selected';} ?>>Power</option>
 <option value="Fly" <?php if($values['course']='Fly') {print 'selected';} ?>>Fly</option>
 <option value="Nichego" <?php if($values['course']='Nichego') {print 'selected';} ?>>Nichego</option>
  </select>
</label>
<?php if (!empty($messages['course'])) {print($messages['course']);} ?>
   <label>
      <p>Биография :</p>
    <textarea name="comment" <?php if ($errors['comment']) {print 'class="error"';} ?> rows="3" cols="40"><?php print $values['comment']; ?></textarea>
          </label>
          <?php if (!empty($messages['comment'])) {print($messages['comment']);} ?>
  <label>
      <p>С контрактом ознакомлен/a</p>
    <input name="check" <?php if ($errors['check']) {print 'class="error"';} ?> checked="<?php if($values['check']) print 'checked'; ?>" type="checkbox" id="check"> Я согласен/а.
        </label>
        <?php if (!empty($messages['check'])) {print($messages['check']);} ?>
  
  <p> <input type="submit" value="Отправить" /></p>
  <p> <button class="exit" hidden="<?php if (empty($_SESSION['login'])) {print('true');} else {print('false');} ?>" onclick="session_destroy()"></button></p>
  </div>
</form>
</div>
</body>
</html>
